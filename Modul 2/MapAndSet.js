const contohArray = [1,'dua',true];
const contohObject = {
    nama:'jonathan',
    kelamin:'pria'
}
// Map
const contohMap = new Map();
contohMap.set('nama','Jonathan');
console.log(contohMap.size); /// ngecek ada berapa keynya
console.log(contohMap.get('nama')); // mendapatkan nilai dari map
console.log(contohMap.has('nama')); //return true karena ada
contohMap.set('kelamin','pria')
console.log(contohMap.has('kelamin'));
console.log(contohMap.delete('kelamin'));
console.log(contohMap.get('kelamin'));// return undefined karena udah dihapus
console.log(contohMap.clear());// dihapus semua keyny
console.log(contohMap.get('nama')); // return undefined jg karena sudah dihapus


const contohMap2 = new Map([
    ['gorengan',500],
    ['nasi goreng',10000],
    ['tempe',2000]
])

    
/// for dari isi(key) map yg bernama contohmap2
for(let makanan of contohMap2.keys()){ // nama makanan nya
    console.log(makanan);
}

for(let valu of contohMap2.values()){
    console.log(valu); // harga2nya
}

for(let keseluruhan of contohMap2.entries()){
    console.log(keseluruhan); // keseluruhan
}


/// set
const contohSet = new Set();
contohSet
.add('joni')
.add('siti')
// .clear()
.delete('joni')
console.log(contohSet.has('siti'));// return true

///for set

const contohSet2 = new Set(["ayam","sapi","kambing"]);

//pake for of
for(let iniBuatan of contohSet2){
    console.log(iniBuatan);
}

// pake for each
contohSet2.forEach((value,valueLagi) => {
    console.log(value);
    console.log(valueLagi);
});
