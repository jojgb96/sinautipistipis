//Destructing

// Array Destructing
const arr = ['a','b','c'];
// const x = arr[0];// cara biasa untuk mendapat
// const y = arr[1];
const[x,y,z] = arr;
console.log(x);

//Object Destructing
const kolot = {
    babeh:'bapak',
    emak:'ibu'
}
const bapaku = kolot.babeh;
console.log(bapaku);
const {babeh,emak} = kolot
console.log(emak);
//rename
const {babeh:bokap,emak:nyokap} = kolot
console.log(bokap);
console.log(nyokap);


/// Pola Nested
let anggota = [
    {nama:'Jonathan', umur:25},
    {nama:'Yola',umur:24}
]
const [{nama:nama1},{nama:nama2}] = anggota;
console.log(nama1);
console.log(nama2);

// Destructing object
const sebuahObject = {
    a:'Ayam',
    b:'Bebek',
    c:'Cacing'
}

function func(object){
    const{a,b,c} = object
}
console.log(func(sebuahObject));

/// Ternary
let kapital = false; //kalo false huruf kecil,kalo besar true
// let hurufA,hurufB,hurufC;
const [hurufA,hurufB,hurufC] = kapital ? ['A','B','C']:['a','b','c'];
console.log(hurufA);

console.log(hurufB);

