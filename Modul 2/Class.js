// ES6

class Kelasku {
    constructor(prop){
        this.prop = prop;
    }

    static classMethod(){ // function
        return 'Kelasku';
    }

    getProp(){
        return this.prop;
    }
}
console.log(Kelasku.classMethod());
const kelasku = new Kelasku(123);
console.log(kelasku.getProp());

console.log("--Inheritance---");
console.log("----------------");

// Inheritance
class Warisan extends Kelasku{
    constructor(prop,warisanProp){
        super(prop * 2);
    }
    static staticMethod(){
        return super.classMethod() + ' Warisan';
    }
}

console.log(Warisan.classMethod());
const warisan = new Warisan(456);
console.log(warisan.getProp());