const tambahan = ['Ayam','Kambing','Babi'];
const kebunBinatang = ['Domba',...tambahan,'Kura-kura','Cacing'];

//kebunBinatang.push('Domba');
console.log(kebunBinatang);

//Pass elemen dari sebuah array sebagai sebuah argumen ke dalam function

function kaliTiga(a,b,c){
    return a*b*c
}
const arr = [2,3,4];
console.log(kaliTiga(...arr));

/// membuat salinan array
const arr1 = [1,2];
const arrBaru = [...arr1];
console.log(arrBaru);
console.log(arr1);

// menggabungkan dua array
const arr2 = ['a','b'];
const arrGabung = [...arr1 , ...arr2];
console.log(arrGabung);

/// Rest Operator
const nomorAwal = 1;
const nomorAkhir = [6,7,8,9,10];

const nomorSemua = [nomorAwal,...nomorAkhir];
console.log(nomorSemua);



